# Congregate Onboarding Workshop

This project is used to enable GitLab and Partner Professional Services Engineers to use the migration automation tool [Congregate](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate).

## Getting started

1. Navigate to the [issue board](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate-onboarding-workshop/-/issues) and create an issue using the **workshop template** :point_down:. Be sure to include **your name** in the issue title.

<img src="img/create-issue.png" width="800">

1. Review the below test system deployment architecture. We will be using Docker Compose to deploy source, destination and congregate docker images :point_down:

<img src="img/migration-workshop-apps.png" width="800">

[graphic source](https://docs.google.com/presentation/d/1Go-psUFefimOI1bHqr81kh-r3J_S_46Z9zFT-Q5nYv4/edit?usp=sharing)
